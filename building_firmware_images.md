Tips to produce Chromebook firmware images
------------------------------------------

On x86 Chromebooks, some proprietary firmware blobs are not available in
the Chromium OS public source tree.  So only Depthcharge can be built
from source, and then it needs to be replaced in a stock firmware image.

## Adapting Depthcharge for LAVA labs

The stock Depthcharge sources may not contain some things that are
necessary so it can be used in a LAVA lab, so when starting with a new
ChromiumOS board we need to do some changes to Depthcharge before we
integrate it in the firmware image.

The first step is identifying a firmware source branch from
https://chromium.googlesource.com/chromiumos/platform/depthcharge/+refs
for our target board.

Then, over this branch we need to check if certain commits from one of
the already working boards are already applied or not and, in case they
aren't, adapt them and apply them. You can find these changes in some of
the branches of https://gitlab.collabora.com/chromium/depthcharge, for
example:
https://gitlab.collabora.com/chromium/depthcharge/-/tree/firmware-octopus-11297.83.B-collabora

Here's a list of the most important ones:

```
0b13bb51 r8152: Support partial reads for jumbo frame ethernet packets
a34dd28a LAVA octopus: CONFIG_CLI and prompt
554b1143 cli: tftpboot: add optional ramdisk argument
d3d7423e net: add ramdisk argument to netboot()
8e55ddcb arch/x86: populate header->ramdisk_{image,size} when provided
714f2285 tftp: allow 16-bit block number to wrap around
c0ffc010 depthcharge:cli: Fix build break
9fcb1ca8 Add driver for RTL8153 based USB network dongles.
f9031a07 cli: tftpboot: enable DHCP for TFTP IP and boot image
b033fabf cli: add tftpboot command
```

These changes add support for Ethernet boot over TFTP, an additional
ramdisk parameter in the tftpboot command, support for RTL8153-based
USB network dongles, such as the one used in the Servo v4 and they also
enable the Depthcharge CLI.

Additionally, for debugging purposes, these commits are interesting to
have:

```
99fee953 DEBUG console_main: print branch name and build date and time
75679b7b DEBUG tftp: print length of downloaded data
```

After these changes are applied and submitted to a repo we want to
build Depthcharge and generate a firmware image for the target. See the
instructions in [README.md](README.md#example-octopus) to have a general
idea about how this is done, specially about the contents of the `.env`
and `.sh` files for each target.

If we're doing this for a target that is already supported in this repo
(ie. it already has a `.env` and a `.sh` file) and we just want to build
Depthcharge using a different source branch, then we need to edit the
appropriate `.sh` file (for example, `octopus.sh`) and update the branch
name in `cmd_checkout()`.

If we're adding support for a new board, then we'd have to create a new
set of `.env` and `.sh` files for it and copy an original firmware
release binary for the board to the `cros-build/firmware` directory. For
the `.env` file you should use the SDK branch selected
[above](#adapting-depthcharge-for-lava-labs). For the `.sh` script you
can start using one of the other ones as a model and then adapt it to
the new board. Most of the build process is identical for all boards,
but take into account that there might be certain board-specific
differences and that the script is supposed to take care of them so that
the process is streamlined for the user. For example, the `rammus` build
failed and the `rammus.sh` script includes a workaround to make it work
(see `cmd_build()` and the patch in `cros-build/setup/rammus_files`).


## Replacing a firmware payload with our modified Depthcharge

A Chromebook firmware binary contains, among other things, a number of Coreboot
payloads. One of them is Depthcharge.

In a CrOS SDK chroot, we can use `cbfstool` to get information about the
contents of a firmware binary and add/replace parts of it, so we can use
it to replace the builtin Depthcharge payload with our modified version.

We have to put Depthcharge in the `COREBOOT` region of the firmware
binary. To check the contents of this region:

```
(cr) cros-build@4d71a209fc9f ~/trunk/src/scripts/firmware $ cbfstool image-bobba-R72-11297.144.0-octopus.serial.bin print -r COREBOOT
Name                           Offset     Type           Size   Comp
cbfs master header             0x0        cbfs header        32 none
fallback/romstage              0x80       stage           71420 none
cpu_microcode_blob.bin         0x117c0    microcode      148480 none
fallback/ramstage              0x35c40    stage          103144 none
config                         0x4ef80    raw              1347 none
revision                       0x4f500    raw               590 none
pt                             0x4f7c0    raw             20480 none
pdpt                           0x54800    raw                32 none
dmic-2ch-48khz-16b.bin         0x54880    raw              3048 none
dmic-4ch-48khz-16b.bin         0x554c0    raw              3048 none
max98357-render-2ch-48khz-24b.bin 0x56100    raw               116 none
dialog-2ch-48khz-24b.bin       0x56200    raw               100 none
fspm.bin                       0x562c0    fsp            417792 none
vbt.bin                        0xbc300    raw              1334 LZMA (5632 decompressed)
wifi_sar_defaults.hex          0xbc880    raw               119 none
locales                        0xbc940    raw               141 LZMA (166 decompressed)
vbt_vortininja.bin             0xbca40    raw              1334 LZMA (5632 decompressed)
fsps.bin                       0xbcfc0    fsp            192512 none
fallback/postcar               0xec000    stage           36196 none
fallback/dsdt.aml              0xf4dc0    raw             12468 none
fallback/verstage              0xf7ec0    stage           65764 none
locale_it.bin                  0x108000   raw              9635 LZMA (285498 decompressed)
locale_lv.bin                  0x10a600   raw             10281 LZMA (273350 decompressed)
locale_kn.bin                  0x10ce80   raw             14197 LZMA (295478 decompressed)
locale_uk.bin                  0x110640   raw             11174 LZMA (270414 decompressed)
locale_ta.bin                  0x113240   raw             12451 LZMA (344586 decompressed)
locale_ca.bin                  0x116340   raw              9586 LZMA (263002 decompressed)
locale_bn.bin                  0x118900   raw              9831 LZMA (235838 decompressed)
locale_ru.bin                  0x11afc0   raw             11585 LZMA (277426 decompressed)
locale_hi.bin                  0x11dd40   raw              9671 LZMA (227154 decompressed)
locale_pt-PT.bin               0x120340   raw              9929 LZMA (269046 decompressed)
locale_en.bin                  0x122a80   raw             21772 LZMA (254422 decompressed)
locale_cs.bin                  0x128000   raw             10502 LZMA (284714 decompressed)
locale_pt-BR.bin               0x12a940   raw             10130 LZMA (296426 decompressed)
locale_da.bin                  0x12d140   raw              9355 LZMA (237498 decompressed)
locale_de.bin                  0x12f640   raw             11178 LZMA (358546 decompressed)
locale_sk.bin                  0x132240   raw             10598 LZMA (283886 decompressed)
locale_lt.bin                  0x134c00   raw              9651 LZMA (251558 decompressed)
locale_th.bin                  0x137200   raw              7940 LZMA (208530 decompressed)
locale_fa.bin                  0x139140   raw              9047 LZMA (236702 decompressed)
locale_et.bin                  0x13b500   raw              9157 LZMA (271114 decompressed)
locale_bg.bin                  0x13d900   raw             11394 LZMA (354726 decompressed)
locale_mr.bin                  0x1405c0   raw              9600 LZMA (219686 decompressed)
locale_zh-CN.bin               0x142b80   raw             12567 LZMA (222274 decompressed)
locale_ms.bin                  0x145d00   raw              9249 LZMA (301506 decompressed)
locale_ja.bin                  0x148180   raw             12763 LZMA (289498 decompressed)
locale_el.bin                  0x14b3c0   raw             12300 LZMA (350198 decompressed)
locale_fi.bin                  0x14e440   raw             10362 LZMA (284366 decompressed)
locale_fil.bin                 0x150d00   raw              9414 LZMA (291282 decompressed)
locale_tr.bin                  0x153200   raw             11016 LZMA (267418 decompressed)
vbgfx.bin                      0x155d40   raw             19861 LZMA (264248 decompressed)
locale_pl.bin                  0x15ab40   raw             10915 LZMA (296054 decompressed)
locale_vi.bin                  0x15d640   raw              9973 LZMA (287206 decompressed)
locale_he.bin                  0x15fd80   raw              8275 LZMA (210254 decompressed)
locale_sr.bin                  0x161e40   raw             11376 LZMA (299958 decompressed)
locale_ko.bin                  0x164b00   raw             10495 LZMA (219358 decompressed)
locale_nl.bin                  0x167440   raw             10859 LZMA (294366 decompressed)
locale_nb.bin                  0x169f00   raw             10487 LZMA (265762 decompressed)
locale_hr.bin                  0x16c840   raw              9672 LZMA (266426 decompressed)
locale_zh-TW.bin               0x16ee40   raw             13025 LZMA (213694 decompressed)
locale_es-419.bin              0x172180   raw             10411 LZMA (292262 decompressed)
locale_ar.bin                  0x174a80   raw              8979 LZMA (197234 decompressed)
locale_es.bin                  0x176e00   raw              9840 LZMA (296938 decompressed)
locale_hu.bin                  0x1794c0   raw             10839 LZMA (336922 decompressed)
font.bin                       0x17bf80   raw              3032 LZMA (61266 decompressed)
locale_ro.bin                  0x17cbc0   raw             10494 LZMA (302726 decompressed)
locale_fr.bin                  0x17f500   raw              9935 LZMA (348818 decompressed)
locale_ml.bin                  0x181c40   raw             14041 LZMA (372710 decompressed)
locale_te.bin                  0x185380   raw             11259 LZMA (246170 decompressed)
locale_id.bin                  0x187fc0   raw              8817 LZMA (274162 decompressed)
locale_sl.bin                  0x18a280   raw             10109 LZMA (296862 decompressed)
locale_gu.bin                  0x18ca40   raw             10238 LZMA (220342 decompressed)
locale_sv.bin                  0x18f280   raw              9931 LZMA (287746 decompressed)
vbt_dorp_hdmi.bin              0x1919c0   raw              1339 LZMA (5632 decompressed)
vbt_blooguard.bin              0x191f80   raw              1334 LZMA (5632 decompressed)
vbt_garg_hdmi.bin              0x192500   raw              1337 LZMA (5632 decompressed)
wifi_sar-bobba360.hex          0x192ac0   raw               119 none
wifi_sar-droid.hex             0x192b80   raw               119 none
fallback/payload               0x192c40   simple elf      85018 none
(empty)                        0x1a78c0   null            46808 none
bootblock                      0x1b2fc0   bootblock       32768 none
```

Note that our modified Depthcharge version might be bigger than the one
in the firmware binary file, so in order to fit it in it could be
necessary to remove some parts of the firmware to create a contiguous
gap that's big enough for it. The locale files are good candidates for
removal because we won't need them for LAVA (we'll use Depthcharge's CLI
exclusively). See the [Sample commands](#sample-commands) section below
for an example.

### Remove other payloads

The firmware images contain many other regions, some of which may also
contain a payload that Coreboot can use. To make sure Coreboot uses only
our payload we need to remove all the others so that it will keep on
looking until it find ours in the COREBOOT region.

```
(cr) cros-build@4d71a209fc9f ~/trunk/src/scripts/firmware $ cbfstool image-bobba-R72-11297.144.0-octopus.serial.bin layout
This image contains the following sections that can be manipulated with this tool:

'SI_DESC' (size 4096, offset 0)
'IFWI' (size 2093056, offset 4096)
'RO_VPD' (size 16384, offset 2097152)
'RO_FRID' (size 64, offset 2115584)
'RO_FRID_PAD' (size 1984, offset 2115648)
'COREBOOT' (CBFS, size 1814528, offset 2117632)
'GBB' (size 262144, offset 3932160)
'RECOVERY_MRC_CACHE' (size 65536, offset 4194304)
'RW_MRC_CACHE' (size 65536, offset 4259840)
'RW_VAR_MRC_CACHE' (size 4096, offset 4325376)
'RW_ELOG' (size 12288, offset 4329472)
'SHARED_DATA' (size 8192, offset 4341760)
'VBLOCK_DEV' (size 8192, offset 4349952)
'RW_VPD' (size 8192, offset 4358144)
'RW_NVRAM' (size 20480, offset 4366336)
'FPF_STATUS' (size 4096, offset 4386816)
'VBLOCK_A' (size 65536, offset 4390912)
'FW_MAIN_A' (CBFS, size 4652992, offset 4456448)
'RW_FWID_A' (size 64, offset 9109440)
'VBLOCK_B' (size 65536, offset 9109504)
'FW_MAIN_B' (CBFS, size 4652992, offset 9175040)
'RW_FWID_B' (size 64, offset 13828032)
'SMMSTORE' (size 262144, offset 13828096)
'RW_LEGACY' (CBFS, size 1835008, offset 14090240)
'BIOS_UNUSABLE' (size 323584, offset 15925248)
'DEVICE_EXTENSION' (size 524288, offset 16248832)
'UNUSED_HOLE' (size 4096, offset 16773120)
```

For example, the `FW_MAIN_A` and `FW_MAIN_B` regions have a payload:

```
(cr) cros-build@4d71a209fc9f ~/trunk/src/scripts/firmware $ cbfstool image-bobba-R72-11297.144.0-octopus.serial.bin print -r FW_MAIN_A | grep payload
fallback/payload               0x10b500   simple elf      85018 none
```
```
(cr) cros-build@4d71a209fc9f ~/trunk/src/scripts/firmware $ cbfstool image-bobba-R72-11297.144.0-octopus.serial.bin print -r FW_MAIN_B | grep payload
fallback/payload               0x10b500   simple elf      85018 none
```

### Sample commands

Here's an example of how to install the Depthcharge payload into a
Coreboot binary, also removing the rest of the payloads that may be
there already. Note that this can be different depending on the initial
firmware binary, so take this as a guideline. Specifically, the removal
of existing payloads and other contents to make room for the new payload
is highly dependent on the binary version and board variant. Once it has
been done for the first time, however, the `.sh` script for the board
should be able to replace the Depthcharge payload for another
automatically using the `image` command (`cmd_image()`).

First, let's copy the original image:

```
cp image-bobba-R72-11297.144.0-octopus.serial.bin exp.bin
```

Then, let's remove the payloads:

```
cbfstool exp.bin remove -r FW_MAIN_A -n fallback/payload
cbfstool exp.bin remove -r FW_MAIN_B -n fallback/payload
cbfstool exp.bin remove -r COREBOOT -n fallback/payload
```

Then, remove enough contiguous locales from the COREBOOT region to leave
a big enough gap for the new Depthcharge payload. For example:

```
cbfstool exp.bin remove -r COREBOOT -n locale_pl.bin
cbfstool exp.bin remove -r COREBOOT -n locale_vi.bin
...
```

We now have:

```
(cr) cros-build@4d71a209fc9f ~/trunk/src/scripts/firmware $ cbfstool exp.bin print -r COREBOOT
Name                           Offset     Type           Size   Comp
cbfs master header             0x0        cbfs header        32 none
fallback/romstage              0x80       stage           71420 none
cpu_microcode_blob.bin         0x117c0    microcode      148480 none
fallback/ramstage              0x35c40    stage          103144 none
config                         0x4ef80    raw              1347 none
revision                       0x4f500    raw               590 none
pt                             0x4f7c0    raw             20480 none
pdpt                           0x54800    raw                32 none
dmic-2ch-48khz-16b.bin         0x54880    raw              3048 none
dmic-4ch-48khz-16b.bin         0x554c0    raw              3048 none
max98357-render-2ch-48khz-24b.bin 0x56100    raw               116 none
dialog-2ch-48khz-24b.bin       0x56200    raw               100 none
fspm.bin                       0x562c0    fsp            417792 none
vbt.bin                        0xbc300    raw              1334 LZMA (5632 decompressed)
wifi_sar_defaults.hex          0xbc880    raw               119 none
locales                        0xbc940    raw               141 LZMA (166 decompressed)
vbt_vortininja.bin             0xbca40    raw              1334 LZMA (5632 decompressed)
fsps.bin                       0xbcfc0    fsp            192512 none
fallback/postcar               0xec000    stage           36196 none
fallback/dsdt.aml              0xf4dc0    raw             12468 none
fallback/verstage              0xf7ec0    stage           65764 none
(empty)                        0x108000   null           318744 none
vbgfx.bin                      0x155d40   raw             19861 LZMA (264248 decompressed)
(empty)                        0x15ab40   null           136216 none
font.bin                       0x17bf80   raw              3032 LZMA (61266 decompressed)
(empty)                        0x17cbc0   null            85464 none
vbt_dorp_hdmi.bin              0x1919c0   raw              1339 LZMA (5632 decompressed)
vbt_blooguard.bin              0x191f80   raw              1334 LZMA (5632 decompressed)
vbt_garg_hdmi.bin              0x192500   raw              1337 LZMA (5632 decompressed)
wifi_sar-bobba360.hex          0x192ac0   raw               119 none
wifi_sar-droid.hex             0x192b80   raw               119 none
(empty)                        0x192c40   null           131928 none
bootblock                      0x1b2fc0   bootblock       32768 none
```

This should give us plenty of room for Depthcharge:

```
(empty)                        0x108000   null           318744 none
```

To add the Depthcharge elf file as a payload:

```
cbfstool exp.bin add-payload -r COREBOOT -n fallback/payload -f /path/to/depthcharge/dev.elf
```

## Testing the image

After the image is generated and flashed to the target board, we should
see the Depthcharge console output and prompt in the serial console (the
same one that Coreboot uses):

```
Starting depthcharge on octopus...
WARNING: can't convert coreboot GPIOs, 'lid' won't be resampled at runtime!
WARNING: can't convert coreboot GPIOs, 'power' won't be resampled at runtime!
BIOS MMAP details:
IFD Base Offset  : 0x1000
IFD End Offset   : 0xf7f000
MMAP Size        : 0xf7e000
MMAP Start       : 0xff082000
The GBB signature is at 0x30004020 and is:  24 47 42 42
octopus:
```

Now, to boot Linux through TFTP we need to use the `tftpboot` command to
load and run a kernel image with specific command line parameters and a
ramdisk. It's recommended to setup a DHCP server in the host to make
this process easier.

Assuming we're running a DHCP server, that there's an Ethernet link
between the chromebook and the host and that the host is configured as a
TFTP server containing a loadable kernel image and an initrd, as well as
a text file containing the kernel command line parameters:

```
$ ls -l /srv/tftp
-rw-r--r-- 1 root root      163 oct 23 15:47 args
-rw-r--r-- 1 root root  9076960 oct 22 16:28 bzImage
-rw-r--r-- 1 root root  5781694 oct 22 17:08 initrd.cpio.gz

$ cat args
earlyprintk=uart8250,mmio32,0xde000000,115200n8 console=ttyS2,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug
```

then we can boot this setup with:

```
> tftpboot dhcp bzImage args initrd.cpio.gz
```

This it will first configure the Ethernet interface in Depthcharge,
fetch the IP configuration via DHCP and boot the kernel with an initrd
and the command line parameters contained in the `args` file.

Note that these command line parameters are target-dependent. In this
case the parameters configure the kernel console and the root filesystem
to mount a NFS volume in the host. You can find suitable kernel command
line parameters for a number of different chromebooks in the [Collabora
LAVA
documentation](https://lava.pages.collabora.com/docs/boards/chromebooks/boards/).
